# "QA in DevOps world" workshop from Nordic Testing Days 2022 conference in Tallinn, Estonia
This project contains all the materials for workshop.

[**Here**](https://youtu.be/mv1sPwcGb48) you can find a video recording of this workshop.
For any questions reach out to [**this**](https://twitter.com/alex_chumakin) twitter account.


## Commands used during the workshop

- Running a new docker container for test project: `docker run -it --rm -v ${PWD}:/app maven:3.8.4-openjdk-17-slim bash`
- Running a new docker container for dev project: `docker run -it --rm -v ${PWD}:/app -v /var/run/docker.sock:/var/run/docker.sock maven:3.8.4-openjdk-17-slim bash`
- Docker installation inside docker container: `apt-get update -qq && apt-get install -y -qq docker.io`
- Build microservice image: `mvn clean package -Dquarkus.container-image.build=true -Dquarkus.container-image.image=rest-fake-name:1.1`
- Run a new container with microservice: `docker run -d -i --rm -p 8080:8080 --name rest-fake-name registry-1.docker.io/library/rest-fake-name:1.1`
- Open locally at http://localhost:8080/name or call endpoint in terminal with `curl localhost:8080/name`
- Run tests in acc environment: `mvn test` or `mvn test -DENV=acc`
- Run tests inside docker container against another docker container: `mvn test -DENV=docker`
- Run tests inside docker container against another docker container with specific bridge network IP address: "mvn test -DENV=docker -DSERVICE_HOST=xxx.xx.x.x"
- Get bridge network IP address (Docker0 IP host): `docker network inspect bridge --format='{{(index .IPAM.Config 0).Gateway}}'`
- Inspect docker container network: docker inspect {C_ID} -f "{{json .NetworkSettings.Networks }}"
- Show containers with their network: docker ps --format "table {{.Image}}\t{{.Ports}}\t{{.Names}}\t{{.Networks}}"
- Build custom image based on existing maven image including docker, git and curl (example is published at dockerhub image `alexandrchumakin/docker-maven-java:1.0`):
```shell
# 'Dockerfile' content
FROM maven:3.8.4-openjdk-17-slim
RUN apt-get update -qq && apt-get install -y -qq docker.io git
```
> - put this Dockerfile at any directory and run `docker build -t {yourDockerhubAccount}/{newImageName}:{tagVersion} .`
> - then login to dockerhub (sign up first if you don't have an account yet) and send this image with `docker push {yourDockerhubAccount}/{newImageName}:{tagVersion}`
